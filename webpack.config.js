const webpack = require('webpack');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const env = process.env.NODE_ENV;
const SRC_DIR = path.resolve(__dirname, './src');
const BUILD_DIR = path.resolve(__dirname, './build');
const MODULES_DIR = path.resolve(__dirname, './node_modules');

const package = require('./package.json');

const baseConfig = {
  entry: {
    app: `${SRC_DIR}/app.js`,
    vendor: Object.keys(package.dependencies)
  },
  output: {
    path: BUILD_DIR,
    chunkFilename: '[name].[chunkhash].js',
    filename: '[name].[chunkhash].js'
  },
  module: {
    rules: [
      {
        // ESLint
        test: /\.js$/,
        exclude: /node_modules/,
        include : SRC_DIR,
        use: {
          loader: 'babel-loader',
        }
      },
      {
        test: /\.html$/,
        exclude: /node_modules/,
        include : SRC_DIR,
        use: {loader: 'html-loader'}
      },
      {
        test: /\.global.css$/,
        exclude: /node_modules/,
        include : SRC_DIR,
        use: [ 'style-loader', 'css-loader' ]
      }
    ]
  },
  resolve: {
    alias: {
      components: path.resolve(__dirname, `${SRC_DIR}/components/`),
      services: path.resolve(__dirname, `${SRC_DIR}/services/`),
      utils: path.resolve(__dirname, `${SRC_DIR}/utils/`),
    },
    extensions: ['.json', '.js', '.css'],
    modules: [ SRC_DIR, MODULES_DIR ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Random thoughts',
      template: `${SRC_DIR}/index.html`,
    }),
    new webpack.HashedModuleIdsPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name:'vendor',
      filename: 'vendor.[chunkhash].js'
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name:'manifest'
    })
  ],
  devServer: {
    historyApiFallback: true,
    port: 8080,
    stats: { colors: true },
  },
};

if (env === 'development') {
  baseConfig.devtool = 'source-map';
}

module.exports = baseConfig;