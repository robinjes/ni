
export const persist = (key, value, action = 'SET') => {
  if (window.localStorage) {
    const prefix = 'ni-'
    switch(action) {
      case 'DEL':
        window.localStorage.removeItem(`${prefix}${key}`);
        return window.localStorage.getItem(`${prefix}${key}`);
      case 'GET':
        const item = window.localStorage.getItem(`${prefix}${key}`)
        if (item && (item.indexOf('[') !== -1) && (item.indexOf(']') !== -1)) return JSON.parse(item)
        return item;
      default:
        window.localStorage.setItem(`${prefix}${key}`, value);
        return window.localStorage.getItem(`${prefix}${key}`);
    }
  }
}