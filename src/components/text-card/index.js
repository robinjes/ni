/* Librairies */
import _template from 'lodash.template';
/* Templates */
import template from './text-card.html';
/* Service */
import { actIt } from 'services';
/* Utils */
import { persist } from 'utils'

class TextCard extends HTMLElement {
  /**
   * Set attributes observer.
   */
  static get observedAttributes() {
    return ['editable', 'changed'];
  }

  get editable() {
    return this.getAttribute('editable');
  }

  get changed() {
    return this.getAttribute('changed');
  }

  set changed(val) {
    if (val) {
      this.setAttribute('changed', 'true')
    } else {
      this.removeAttribute('changed')
    }
  }



  /**
   * On attribute change.
   */
  attributeChangedCallback() {
    const contentEl = this.shadowRoot.getElementById(`${this.id}-content`)
    const titleEl = this.shadowRoot.getElementById(`${this.id}-title`)
    const actionEl = this.shadowRoot.getElementById(`${this.id}-actions`)
    const saveicon = this.shadowRoot.querySelector('text-icon[icon-type="save"]')
    if (saveicon && (this.editable === 'true')) {
      saveicon.btn.classList.remove('hide'); // show save btn
    } else {
      saveicon.btn.classList.add('hide'); // hide save btn
    }

    if (contentEl && titleEl && (this.editable === 'true')) {
      contentEl.setAttribute('contenteditable', this.editable)
      titleEl.setAttribute('contenteditable', this.editable)
    } else {
      contentEl.removeAttribute('contenteditable')
      titleEl.removeAttribute('contenteditable')
    }
   
    if (saveicon && this.changed) {
      saveicon.btn.classList.add('unsaved');
    } else {
      saveicon.btn.classList.remove('unsaved');
    }

    if ((this.id === 'NEW') && contentEl && titleEl && (this.editable === 'true')) {
      contentEl.classList.remove('hide')
      titleEl.classList.remove('hide')
      actionEl.classList.remove('hide')
    } else if (this.id === 'NEW') {
      contentEl.classList.add('hide')
      titleEl.classList.add('hide')
      actionEl.classList.add('hide')
    }

  }

  /**
   * Fetch stored data on component mount.
   */
  connectedCallback() {
    this.attachShadow({ mode: 'open' });

    this.id = this.getAttribute('text-id');
    const version = this.getAttribute('text-version');
    const type = this.getAttribute('text-type');
    const data = window.store.texts.content[`${this.id}_${version}`]
    const infos = window.store.texts.info[this.id]
  
    if (data && this.id !== 'NEW') {
      this.renderText(this.id, type, data, infos);
    }

    if (this.id === 'NEW') {
      this.renderNewText(this.id, type)
    }

  }

  /**
   * Render text-icon components.
   */
  renderIcon(id, type, nb = 0, highlight = false) {
    const icon = document.createElement('SPAN');

    switch(type) {
      case 'notes':
      case 'music_notes':
        icon.innerHTML = `<text-icon icon-id="${id}" icon-type="${type}" icon-highlighted="${highlight}"></text-card>`
        if (!highlight) {
          icon
            .addEventListener('click', () => {
              this.shadowRoot.getElementById(`${id}-type`).value = type
            })
        }
        return icon
      case 'save':
        icon.innerHTML = `<text-icon icon-id="${id}" icon-type="${type}" icon-highlighted="${highlight}"></text-card>`
        if (!highlight) {
          icon
            .addEventListener('click', () => {
              if (this.changed) {
                const titletosave = this.shadowRoot.getElementById(`${id}-title`).innerHTML || '';
                const contenttosave = this.shadowRoot.getElementById(`${id}-content`).innerHTML || '';
                const typetosave = this.shadowRoot.getElementById(`${id}-type`).value || 'text';
                const isLogged = persist('uid', null, 'GET');
                actIt('save', isLogged, id, titletosave, contenttosave, typetosave)
                  .then(() => {
                    this.changed = false
                  })
                  .catch((res) => {
                    window.store.errors.push(res.error)
                  })
              }
            })
        }
        return icon

      default :
        // like/dislike
        icon.innerHTML = `<text-icon icon-id="${id}" icon-type="${type}" icon-digits="${nb}" icon-highlighted="${highlight}"></text-card>`
        if (!highlight) {
          icon
            .addEventListener('click', () => {
              const isLogged = persist('uid', null, 'GET');
              if (isLogged) {
                actIt(type, isLogged, id)
                  .then(() => {
                    const iconEl = this.shadowRoot.querySelector(`text-icon[id="${id}"][icon-type="${type}"]`);
                    if (iconEl) {
                      iconEl.setAttribute('digits', (parseInt(iconEl.getAttribute('digits'), 10) + 1).toString())
                      iconEl.setAttribute('highlighted', 'true');
                    }
                  })
                  .catch((res) => {
                    window.store.errors.push(res.error)
                  })
              } else {
                const loginEl = document.querySelector('app-login')
                if (loginEl) loginEl.setAttribute('isopen', true) // open login modal
              }
            })
        }
        return icon
    }

  }

  /**
   * Render content when data fetched.
   */
  renderText(id, type, { title, content }, { likes, shits }) {
    const compiled = _template(template);
    const fetchedTemplate = compiled({ id, type, title, content: content.replace(new RegExp('\n', 'g'),'</br>') });
    const HTMLTextCardTemplate = new DOMParser()
      .parseFromString(fetchedTemplate, 'text/html')
      .querySelector('template');

    const child = HTMLTextCardTemplate.content.cloneNode(true)
    this.shadowRoot.appendChild(child);

    const contentEl = this.shadowRoot.getElementById(`${id}-content`)
    const titleEl = this.shadowRoot.getElementById(`${id}-title`)
    const iconsEl = this.shadowRoot.getElementById(`${id}-actions`)

    contentEl.dataset.type = type
    contentEl
      .addEventListener('input', () => {
        // set 'changed' on text changes
        this.changed = true
      })
    titleEl
      .addEventListener('input', () => {
        // set 'changed' on title changes
        this.changed = true
      })

    const lovedTexts = persist('lovedTexts', null, 'GET') || [];
    const shatTexts = persist('shatTexts', null, 'GET') || [];

    iconsEl.appendChild(this.renderIcon(id, 'save'));
    iconsEl.appendChild(this.renderIcon(id, 'favorite', likes, lovedTexts.indexOf(id) !== -1));
    iconsEl.appendChild(this.renderIcon(id, 'close', shits, shatTexts.indexOf(id) !== -1));
  }

  renderNewText(id, type) {
    const compiled = _template(template);
    const fetchedTemplate = compiled({ id, type, title: 'New title.', content: 'New text.' });
    const HTMLTextCardTemplate = new DOMParser()
      .parseFromString(fetchedTemplate, 'text/html')
      .querySelector('template');

    const child = HTMLTextCardTemplate.content.cloneNode(true)
    this.shadowRoot.appendChild(child);

    const contentEl = this.shadowRoot.getElementById(`${id}-content`)
    const titleEl = this.shadowRoot.getElementById(`${id}-title`)
    const iconsEl = this.shadowRoot.getElementById(`${id}-actions`)

    contentEl.classList.add('new')
    contentEl.dataset.type = type
    contentEl
      .addEventListener('input', () => {
        // set 'changed' on text changes
        this.changed = true
      })

    titleEl.classList.add('new')
    titleEl
      .addEventListener('input', () => {
        // set 'changed' on title changes
        this.changed = true
      })
    
    iconsEl.appendChild(this.renderIcon('NEW', 'save'));
    iconsEl.appendChild(this.renderIcon('NEW', 'notes'));
    iconsEl.appendChild(this.renderIcon('NEW', 'music_notes'));

  }
}

window.customElements.define('text-card', TextCard);