
/* Templates */
import template from './text-icon.html';

class TextIcon extends HTMLElement {
  /**
   * Set attributes observer.
   */
  static get observedAttributes() {
    return ['digits', 'editable', 'highlighted'];
  }

  get digits() {
    return this.getAttribute('digits');
  }

  set digits(val) {
    this.setAttribute('digits', val);
  }

  get editable() {
    return this.getAttribute('editable');
  }

  set editable(val) {
    this.setAttribute('editable', val);
  }

  get highlighted() {
    return this.getAttribute('highlighted');
  }

  set highlighted(val) {
    this.setAttribute('highlighted', val);
  }

  /**
   * On attribute change.
   */
  attributeChangedCallback() {
    this.shadowRoot.querySelector('.text-icon-digits').innerHTML = this.digits
    this.shadowRoot.querySelector('i').dataset.highlighted = this.highlighted
  }


  connectedCallback() {
    this.attachShadow({ mode: 'open' });
    const HTMLTemplate = new DOMParser()
      .parseFromString(template, 'text/html')
      .querySelector('template');

    this.shadowRoot.appendChild(HTMLTemplate.content.cloneNode(true))

    this.enabled = this.getAttribute('icon-enabled')
    this.type = this.getAttribute('icon-type')
    this.id = this.getAttribute('icon-id')
    this.digits = this.getAttribute('icon-digits') || ''
    this.highlighted = this.getAttribute('icon-highlighted') || ''

    this.btn = this.shadowRoot.getElementById('text-icon-i')
    this.btn.id = `${this.btn.id}-${this.id}`

    this.render(this.type)

  }

  /**
   * Render content when data fetched.
   */
  render() {
    this.btn.id = `${this.btn.id}-${this.id}`
    this.btn.dataset.type = this.type;
    this.btn.dataset.enabled = this.enabled;
    this.btn.innerHTML = this.type
    if (this.digits !== '') this.shadowRoot.querySelector('.text-icon-digits').innerHTML = this.digits
    this.shadowRoot.querySelector('[data-type]').dataset.highlighted = this.highlighted;
  }
}

customElements.define('text-icon', TextIcon);