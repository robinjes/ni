/* Templates */
import template from './app-content.html';
/* Service */
import { fetchTextsInfo, fetchTextsContent } from 'services';

/* Render */
class AppContent extends HTMLElement {
  /**
   * Set attributes observer.
   */
  static get observedAttributes() {
    return ['fetched', 'editable'];
  }

  /**
   * A getter for a fetched property.
   */
  get fetched() {
    return this.hasAttribute('fetched');
  }

  /**
   * A getter for a fetched property.
   */
  get editable() {
    return this.getAttribute('editable');
  }

  /**
   * A setter for a fetched property.
   */
  set fetched(val) {
    // Reflect the value of the fetched property as an HTML attribute.
    if (val) {
      this.setAttribute('fetched', '');
    } else {
      this.removeAttribute('fetched');
    }
  }

  /**
   * A setter for a fetched property.
   */
  set editable(val) {
    // Reflect the value of the fetched property as an HTML attribute.
    if (val === 'true') {
      this.setAttribute('editable', 'true');
    } else {
      this.removeAttribute('editable', 'false');
    }
  }

  /**
   * On attribute change.
   */
  attributeChangedCallback() {
    if (this.fetched) {
      this.renderContent()
    }
    const textCardsEls = this.shadowRoot.querySelectorAll('text-card')
    if (this.editable === 'true') {
      textCardsEls.forEach((el) => {
        el.setAttribute('editable', true)
      })
    } else {
      textCardsEls.forEach((el) => {
        el.setAttribute('editable', false)
      })
    }
  }

  /**
   * Fetch remote data on component mount.
   */
  connectedCallback() {
    this.attachShadow({ mode: 'open' });
    this.renderContainer();
    fetchTextsInfo()
      .then((res_a) => {
        window.store.texts.info = res_a.content;
        fetchTextsContent()
          .then((res_b) => {
            window.store.texts.content = res_b.content;
            this.setAttribute('fetched', '');
          });
    });
  }

  /**
   * Render container
   */

  renderContainer() {
    const HTMLTemplate = new DOMParser()
      .parseFromString(template, 'text/html')
      .querySelector('template');

    this.shadowRoot.appendChild(HTMLTemplate.content.cloneNode(true));
  }

  /**
   * Render content when data fetched.
   */
  renderContent() {
    const container = this.shadowRoot.getElementById('app-content_content')
    container.classList.remove('fetching');
    container.innerText = ''
    for (let key in window.store.texts.info) {
      const { id, type, lastVersion } = window.store.texts.info[key]
      container.appendChild(this.renderTextCard(id, type, lastVersion));
    }
    container.appendChild(this.renderTextCard('NEW', 'text'));
  }

  /**
   * Render text-card component.
   * @param {string} id  - text id.
   * @param {string} lastVersion - text version
   */
  renderTextCard(id, type, lastVersion) {
    const card = document.createElement('DIV');
    card.setAttribute('class', 'app-content_item')
    card.innerHTML = `<text-card text-id="${id}" text-version="${lastVersion || ''}" text-type="${type}"></text-card>`
    return card;
  }

}

window.customElements.define('app-content', Object.freeze(AppContent));
