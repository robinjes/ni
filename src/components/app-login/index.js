/* Templates */
import template from './app-login.html';
/* Service */
import { login } from 'services';
/* Utils */
import { persist } from 'utils'

/* Render */
class AppLogin extends HTMLElement {
  /**
   * Set attributes observer.
   */
  static get observedAttributes() {
    return ['isopen', 'islogged'];
  }

  /**
   * A getter for a fetched property.
   */
  get isopen() {
    return this.getAttribute('isopen') === 'true';
  }

  get islogged() {
    return this.getAttribute('islogged') === 'true';
  }

  /**
   * On attribute change.
   */
  attributeChangedCallback() {
    const el = this.shadowRoot.getElementById('app-login-form')
    if (el && this.isopen) {
      el.classList.remove('hide')
      this.shadowRoot.getElementById('app-login-action-login').classList.add('hide')
    } else if (el && !this.isopen) {
      el.classList.add('hide')
      this.shadowRoot.getElementById('app-login-action-login').classList.remove('hide')
    }

    if (el && this.islogged) {
      this.shadowRoot.getElementById('app-login-action-login').classList.add('hide')
      this.shadowRoot.getElementById('app-login-action-logout').classList.remove('hide')
    } else {
      this.shadowRoot.getElementById('app-login-action-login').classList.remove('hide')
      this.shadowRoot.getElementById('app-login-action-logout').classList.add('hide')
    }
  }

  connectedCallback() {
    this.attachShadow({ mode: 'open' });
    this.render();
    this.setAttribute('isopen', 'false');
    this.autoLogin()
  }

  addListeners() {
    // on send
    this.shadowRoot
      .getElementById('app-login-btn-send')
      .addEventListener('click', e => {
        e.preventDefault();
        const email = this.shadowRoot.getElementById('app-login-input').value;
        login(email)
          .then(({ success, content }) => {
            if (success) this.setAsLogged(content)
            if (!success) {
              window.store.errors.push(content)
            }
            this.setAttribute('isopen', 'false');
          })
          .catch(({ error }) => {
            this.setAsUnlogged()
            window.store.errors.push(error)
            this.setAttribute('isopen', 'false');
          })
      });

    // on open
    this.shadowRoot
      .getElementById('app-login-action-login')
      .addEventListener('click', e => {
        this.setAttribute('isopen', 'true');
      });

    // on close
    this.shadowRoot
      .getElementById('app-login-btn-close')
      .addEventListener('click', e => {
        this.setAttribute('isopen', 'false');
      });

    // on unconnect
    this.shadowRoot
      .getElementById('app-login-action-logout')
      .addEventListener('click', e => {
        this.setAsUnlogged();
      });
  }

  autoLogin() {
    const uid = persist('uid', null, 'GET')
    if(uid) {
      login(uid)
        .then(({ success, content }) => {
          if (success) this.setAsLogged(content)
          if (!success) {
            window.store.errors.push(content)
          }
          this.setAttribute('isopen', 'false');
        })
        .catch(({ error }) => {
          this.setAsUnlogged()
          window.store.errors.push(error)
          this.setAttribute('isopen', 'false');
        })
    }
  }

  setAsLogged({ grp }) {
    this.setAttribute('islogged', 'true');
    const appcontentEl = document.querySelector('app-content')
    if (appcontentEl) appcontentEl.setAttribute('editable', grp === 'ADMIN' ? 'true' : 'false')
  }

  setAsUnlogged() {
    persist('uid', null, 'DEL')
    persist('grp', null, 'DEL')
    persist('lovedTexts', null, 'DEL')
    persist('shatTexts', null, 'DEL')
    window.location.reload();
  }

  render() {
    const HTMLTemplate = new DOMParser()
      .parseFromString(template, 'text/html')
      .querySelector('template');

    this.shadowRoot.appendChild(HTMLTemplate.content.cloneNode(true))

    this.addListeners();
  }

}

window.customElements.define('app-login', Object.freeze(AppLogin));
