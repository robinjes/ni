
/* Librairies */
import _template from 'lodash.template';
import moment from 'moment';
/* Templates */
import template from './app-header.html';
/* Service */
import { fetchContext } from 'services';

/* Render */
class AppHeader extends HTMLElement {
  /**
   * Set attributes observer.
   */
  static get observedAttributes() {
    return ['fetched'];
  }

  /**
   * A getter for a fetched property.
   */
  get fetched() {
    return this.hasAttribute('fetched');
  }

  /**
   * A setter for a fetched property.
   */
  set fetched(val) {
    // Reflect the value of the fetched property as an HTML attribute.
    if (val) {
      this.setAttribute('fetched', '');
    } else {
      this.removeAttribute('fetched');
    }
  }

  /**
   * On attribute change.
   */
  attributeChangedCallback() {
    this.shadowRoot.innerHTML = ''
    if (this.fetched) {
      this.render()
    }
  }

  /**
   * Fetch remote data on component mount.
   */
  connectedCallback() {
    this.attachShadow({ mode: 'open' });
    fetchContext()
      .then((res_a) => {
        window.store.context = res_a.content;
        this.setAttribute('fetched', '');
    });
  }

  /**
   * Render content when data fetched.
   */
  render() {
    const compiled = _template(template);
    const fetchedTemplate = compiled({ title: window.store.context.title, date: moment().format("MMM Do YYYY") });
    const HTMLTemplate = new DOMParser()
      .parseFromString(fetchedTemplate, 'text/html')
      .querySelector('template');

    this.shadowRoot.appendChild(HTMLTemplate.content.cloneNode(true))
  }

}

window.customElements.define('app-header', Object.freeze(AppHeader));
