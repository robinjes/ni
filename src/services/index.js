/* Librairies */
import axios from 'axios';
/* Utils */
import { persist } from 'utils'

export const fetchContext = () => (
  new Promise((resolve) => {
    axios.get('https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_context')
      .then(({ data }) => {
        window.fetched['context'] = true;
        resolve(data)
      })
      .catch((err) => {
        console.error(err)
      });
  })
);

export const fetchTextsInfo = () => (
  new Promise((resolve) => {
    axios.get('https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_texts_infos')
      .then(({ data }) => {
        window.fetched['texts-info'] = true;
        resolve(data)
      })
      .catch((err) => {
        console.error(err)
      });
  })
);

export const fetchTextsContent = () => (
  new Promise((resolve) => {
    axios.get('https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_texts_contents')
      .then(({ data }) => {
        window.fetched['texts-content'] = true;
        resolve(data)
      })
      .catch((err) => {
        console.error(err)
      });
  })
);

export const login = (email) => (
  new Promise((resolve, reject) => {
    axios.get(`https://us-central1-random-thoughts-787bb.cloudfunctions.net/get_login?email=${email}`)
      .then(({ data }) => {
        window.fetched['login'] = true;
        const { success, content } = data;
        if (success) {
          const { id, grp, lovedTexts, shatTexts } = content;
          persist('uid', id, 'SET')
          persist('grp', grp, 'SET')
          persist('lovedTexts', JSON.stringify(lovedTexts), 'SET')
          persist('shatTexts', JSON.stringify(shatTexts), 'SET')
        }
        resolve(data)
      })
      .catch((err) => {
        reject(err)
      });
  })
);

export const actIt = (action, email, textid, title, content, type) => (
  new Promise((resolve) => {
    const data = {
      email,
      textid
    }

    if (action === 'save') {
      data.title = title
      data.text = content
      data.type = type
    }

    axios({
      url: `https://us-central1-random-thoughts-787bb.cloudfunctions.net/set_${action}`,
      method: 'put',
      data
    })
      .then(({ data }) => {
        resolve(data)
      })
      .catch((err) => {
        console.error(err)
      });
  })
);
