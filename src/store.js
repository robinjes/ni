// Initialize store
window.store = {
  context: {},
  texts: {
    info: {},
    content: {},
  },
  errors: []
}

window.fetched = {
  'context': false,
  'texts-info': false,
  'texts-content': false,
  'login': false,
}
