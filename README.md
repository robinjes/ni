# About
## Goal
Make a refined and responsive single page website which will contain 'Natural Intelligence' texts.
## Technos
The aim of this project is to experiment with native web components.

# Run
## Dev. mode
```npm run start```

Go to [http://localhost:8080](http://localhost:8080)

## Build mode
```npm run build```

Go to 'build/'

# Ressources
## Backend stack
The backend has been developed to be deployed on Firebase, using its database and cloud functions services (see [ni-functions](https://gitlab.com/robinjes/ni-functions)).

## Useful links
- [Web components polyfills](https://www.webcomponents.org/polyfills)
- [Building Components](https://developers.google.com/web/fundamentals/web-components/)
- [HTML Web Component using Vanilla JavaScript - Part 1](https://ayushgp.github.io/html-web-components-using-vanilla-js/)
- [HTML Web Component using Vanilla JavaScript - Part 2](https://ayushgp.github.io/html-web-components-using-vanilla-js-part-2/)
- [HTML Web Component using Vanilla JavaScript - Part 3](https://ayushgp.github.io/html-web-components-using-vanilla-js-part-3/)
- [Web Components by Example: Part 1](https://codeburst.io/web-components-by-example-part-1-db05681eb6a)
- [The web components experience](https://jeremenichelli.io/2017/10/the-web-components-experience/)
- [babel-plugin-transform-custom-element-classes](https://github.com/github/babel-plugin-transform-custom-element-classes/blob/master/README.md)
- [Observing changes to attributes](https://developers.google.com/web/fundamentals/web-components/customelements#attrchanges)
- [Styling Web Components Using A Shared Style Sheet](https://www.smashingmagazine.com/2016/12/styling-web-components-using-a-shared-style-sheet/)
- [A Guide to Custom Elements for React Developers](https://css-tricks.com/a-guide-to-custom-elements-for-react-developers/?utm_campaign=React%2BNewsletter&utm_medium=email&utm_source=React_Newsletter_139)